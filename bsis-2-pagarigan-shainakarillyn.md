# Aspiring Business / Systems Analyst

## Shaina Karillyn G. Pagarigan

👋 Aspiring Business / Systems Analyst — 💌 shainakarillynpagarigan@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_pagarigan_shainakarillyn.jpg](images/bsis_2_pagarigan_shainakarillyn.jpg)

### Bio

**Good to know:** I am fond of watching korean dramas and love listening to kpop especially the songs from NCT and EXO. Recently, I enjoy watching Formula One (F1) and simps for Scuderia Ferrari (CL16 & CS55) because I like the way on how cars can get that fast.

**Motto:** "Always believe that you should never, ever give up and you should always keep fighting even when there's only a slightest chance."

**Languages:** Basic Java, HTML, CSS

**Other Technologies:** Microsofr Office, Canva, Replit, VS Code

**Personality Type:** [Architect (INTJ-T)](https://www.16personalities.com/profiles/3b5e4b840a951)

<!-- END -->